window.addEventListener('DOMContentLoaded', () => {
    const examplesButtons = document.querySelectorAll(".examples__button_white")
    const button1 = document.querySelector(".examples__button-all")
    button1.classList.add('examples__button_black')
    const button2 = document.querySelector(".examples__button-key")
    const button3 = document.querySelector(".examples__button-razrabotka")
    const button4 = document.querySelector(".examples__button-design")
    const doublepic1 = document.querySelector('.examples__pictures__doublepic1')
    const doublepic2 = document.querySelector('.examples__pictures__doublepic2')
    const sunsetPic = document.querySelector('.examples__pictures__sunset')
    const bublesPic =  document.querySelector(".examples__pictures__bubles")
    const bedsPic = document.querySelector(".examples__pictures__beds")
    const strizhiPic = document.querySelector(".examples__pictures__strizhi")
    const rembrandtPic = document.querySelector(".examples__pictures__rembrandt")
    const magnitophonePic = document.querySelector(".examples__pictures__magnitophone")

    const countResult = document.querySelector(".count__result")
    const resultButton = document.querySelector(".count__form__calculation")
    countResult.style.display = 'none'

examplesButtons.forEach((element) => {
    element.classList.remove('examples__button_black')
})
examplesButtons[0].classList.add('examples__button_black')

    examplesButtons.forEach((element) => {
        element.addEventListener('click', (e) => {
            examplesButtons.forEach((removeBlack) => {
                removeBlack.classList.remove('examples__button_black')
            })
            e.target.classList.add('examples__button_black')
        })
    })
    button1.addEventListener('click', (funcButton1) => {
        sunsetPic.style.margin = "0"
        doublepic1.style.margin = "40px 0px"
        doublepic2.style.margin = "40px 0px"
        bedsPic.style.display = 'block'
        strizhiPic.style.display = 'block'
        sunsetPic.style.display = 'block'
        bublesPic.style.display = 'block'
        magnitophonePic.style.display = "block"
        rembrandtPic.style.display = "block"
    })
    button2.addEventListener('click', (funcButton2) => {
        sunsetPic.style.margin = "0"
        doublepic1.style.margin = "0"
        doublepic2.style.margin = "40px 0px"
        rembrandtPic.style.display = "none"
        magnitophonePic.style.display = "none"
        sunsetPic.style.display = "none"
        bublesPic.style.display = "none"
        bedsPic.style.display = 'block'
        strizhiPic.style.display = 'block'
        bedsPic.style.width = "100%" 
        strizhiPic.style.display = "100%"
    })
    button3.addEventListener('click', (funcButton3) => {
        sunsetPic.style.margin = "0"
        doublepic1.style.margin = "0"
        doublepic2.style.margin = "40px 0px"
        magnitophonePic.style.display = "block"
        bublesPic.style.display = "block"
        strizhiPic.style.display = "none"
        rembrandtPic.style.display = "none"
        sunsetPic.style.display = "none"
        bedsPic.style.display = "none" 
        magnitophonePic.style.width = "100%"
        bublesPic.style.width = "100%"
    })
    button4.addEventListener('click', (funcButton4) => {
        sunsetPic.style.margin = "40px 0px"
        doublepic2.style.magin = "0"
        doublepic2.style.margin = "0"
        sunsetPic.style.display = "block"
        rembrandtPic.style.display = "block"
        strizhiPic.style.display = "none"
        magnitophonePic.style.display = "none"
        bublesPic.style.display = "none"
        bedsPic.style.display = "none" 
        sunsetPic.style.width = "100%"
        rembrandtPic.style.width = "100%"
    })
    resultButton.addEventListener('click', (resultButton) => {
        if (countResult.style.display.includes('block')) {
            countResult.style.display = 'none'
          }else {
            countResult.style.display = 'block'
          }
    })
    let qaButton = document.querySelectorAll('.qa__part-line__button')
qaButton.forEach((element) => {
  element.addEventListener('click', (e) => {
    let qaInvisibleLine = element.querySelector('.qa__invisible-line')
    let plus = element.querySelector('.qa__visible-line p2')
    if (qaInvisibleLine.style.display.includes('flex')) {
      qaInvisibleLine.style.display = 'none'
      plus.innerHTML = "+"
    }else {
      qaInvisibleLine.style.display = 'block'
      qaInvisibleLine.style.display = "flex"
      plus.innerHTML = "-"
    }
  })
})
})

